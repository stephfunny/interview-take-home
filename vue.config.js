'use strict';
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
    outputDir: 'dist',
    configureWebpack: {
        plugins: [
            new CopyWebpackPlugin([
                // copy custom static assets (e.g., Netlify _redirects)
                {
                    from: path.resolve(__dirname, './distRoot'),
                    to: '',
                    ignore: ['.*', '*.MD']
                }
            ]),
            // new VuetifyLoaderPlugin()

        ]
    }

};
