module.exports = {
  presets: [
    ['@vue/app', { useBuiltIns: 'entry' }]
  ],
  env: {
  production: {
    plugins: ['transform-remove-console']
  }
}
}
