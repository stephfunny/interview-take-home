import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

require('dotenv').load();
import Trend from 'vuetrend';

Vue.use(Trend);
import ApiService from './common/api.service';
import AttomApiService from './common/attom.api.service';

const GOOGLE_MAPS_API_KEY = process.env.VUE_APP_GOOGLE_MAPS_API_KEY;

Vue.config.productionTip = false
Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: GOOGLE_MAPS_API_KEY,
});

ApiService.init();
AttomApiService.init();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
