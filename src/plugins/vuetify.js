import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/src/stylus/theme.styl';
import 'vuetify/src/stylus/app.styl';
import '@mdi/font/css/materialdesignicons.css';

Vue.use(Vuetify, {
  options: {
  customProperties: true
  },

  // these are the currently used company colors, most notably primary purple
  theme: {
    primary: '#195658',
    secondary: '#2D797D',
    accent: '#E29E23',
    error: '#D1001A',
    success: '#23947A',
    warning: '#FFC107'
  },
  iconfont: 'mdi'
});
