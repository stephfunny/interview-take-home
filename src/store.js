import Vue from 'vue'
import Vuex from 'vuex'
import ApiService from '@/common/api.service';
import AttomApiService from '@/common/attom.api.service';
import xmlToJsonFunct from '@/common/xmlToJson';
require('dotenv').load();
Vue.use(Vuex);
var xmlToJson = new xmlToJsonFunct();
// import zComps from './data/zillowComps.json';
// import zDetails from './data/zillowDetails.json';
// import zSearch from './data/zillowSearch.json';
// import attomData from './data/attomData.json';
// import attomAssessment from './data/attomAssessment.json';
// If you want to use Vuex, a state management pattern https://vuex.vuejs.org/

const getDefaultState = () => {
  return {
    // zpid: 48749425,
    // zDetails: zDetails.updatedPropertyDetails.response,
    // zResults: [zSearch.response.results.result],
    // attomSalesHistory: attomData.property[0],
    // attomAssessment: attomAssessment,
    // zComps: zComps.comp,
    // zCompsDetails: zComps.principal,
    // showComps: true,
    showComps: false,
    zpid: 0,
    zDetails: {},
    zComps: [],
    zResults: [],
    attomAssessment: {},
    attomSalesHistory: [],
    zCompsDetails: {},
    zStatusCode: 0,
    searchPlace: '',
    place: {},
    loading: false,
    errored: false,
    errorMessage: '',
    inputQuery: {},
  }
}
const state = getDefaultState();

export default new Vuex.Store({
  state,

  getters: {
    zpid(state) {
      return state.zpid;
    },
    zDetails(state) {
      return state.zDetails;
    },
    zResults(state) {
      return state.zResults;
    },
    zComps(state) {
      return state.zComps;
    },
    showComps(state) {
      return state.showComps;
    },
    loading(state) {
      return state.loading;
    },
    attomAssessment(state) {
      return state.attomAssessment;
    },
    attomSalesHistory(state) {
      return state.attomSalesHistory;
    },
    zStatusCode(state) {
      return state.zStatusCode;
    },
    inputQuery(state) {
      return state.inputQuery;
    },
    errored(state) {
      return state.errored;
    },
  },

  mutations: {
    resetState (state) {
      Object.assign(state, getDefaultState());
    },
    setZpid(state, id) {
      Object.assign(state, getDefaultState());
      state.zpid = id;
    },
    setZDetails(state, data) {
      state.errored = false;
      state.zDetails = data;
    },
    setZComps(state, data) {
      state.errored = false;
      state.zComps = data;
    },
    setZCompsDetails(state, data) {
      state.errored = false;
      state.zCompsDetails = data;
    },
    setZResults(state, data) {
      state.zResults = data;
      state.zStatusCode = 0;
      if (data.length === 1) {
        state.zpid = data[0].zpid;
      }
    },
    setLoading(state, bool) {
      state.loading = bool;
    },
    setErrored(state, bool) {
      state.errored = bool;
    },
    setAttomAssessment(state, data) {
      state.attomAssessment = data;
    },
    setAttomSalesHistory(state, data) {
      state.attomSalesHistory = data;
    },
    setZStatusCode(state, data) {
      state.zStatusCode = data;
    },
    setInputQuery(state, inputQuery) {
      state.inputQuery = inputQuery
    },
    setInputQueryFormatted(state, inputQuery) {
      state.inputQuery.street = inputQuery.street;
      state.inputQuery.zip = inputQuery.zip;
    },
    setInputQueryString(state, inputQuery) {
      state.inputQuery.queryString = inputQuery
    },
  },

  actions: {
    resetState ({ commit }) {
      commit('resetState');
    },

    inputQuery ({commit, dispatch, state}, input) {
      if (input.street && input.zip) {
        commit('setInputQueryFormatted', input);
        dispatch('getSearchResult', {
          address: input.street,
          citystatezip: input.zip,
        })
      } else {
        // check if we have inputQuery in correct format already
        commit('setInputQueryString', input.queryString);
        if (state.inputQuery.street && state.inputQuery.zip) {
          dispatch('getSearchResult', {
            address: state.inputQuery.street,
            citystatezip: state.inputQuery.zip,
          })
        } else {
          // triggered by click and we don't have formatted address
          commit('setErrored', true);
        }
      }
    },

    setZData({commit}, payload) {
      commit('setZResults', payload);
    },

    getPropertyDetails({commit}, payload) {
      commit('setLoading', true);

      ApiService.getZillowData('GetUpdatedPropertyDetails.htm', {
          zpid: payload,
          rentzestimate: true,
        })
        .then(({
          data
        }) => {
          var json = xmlToJson.xml_str2json(data);
          if (!json.updatedPropertyDetails) {
            return;
          } else {
            var res = json.updatedPropertyDetails;
            if (res.response) {
              commit('setZDetails', res.response);
            }
          }
        })
        .catch(err => {
          console.log('Error in GetUpdatedPropertyDetails', err);
        })
        .finally(() => {
          commit('setLoading', false);
        });
        ApiService.getZillowData('GetDeepComps.htm', {
            zpid: payload,
            count: 25,
            rentzestimate: true,
          })
          .then(({
            data
          }) => {
            var json = xmlToJson.xml_str2json(data);
              var res = json.comps;
              commit('setZComps', res.response.properties.comparables.comp);
          })
          .catch(err => {
            console.log('Error in GetDeepComps', err);
          })
          .finally(() => {
            commit('setLoading', false);
          })
    },

    getSearchResult({commit, dispatch}, payload) {
      commit('setLoading', true);
      return ApiService.getZillowData('GetSearchResults.htm', payload)
        .then(({data}) => {
          var json = xmlToJson.xml_str2json(data);
          var searchResults = json.searchresults;

          if (!searchResults || !searchResults.message || !searchResults.request) {
            commit('setErrored', true);
            return;
          }
          commit('setErrored', false);
          commit('setZStatusCode', searchResults.message.code);
          if (searchResults.message.code == 0) {
            var results = searchResults.response.results.result;
            /* Now check if Zillow returned one result or multiple. */
            if (Array.isArray(results)) {
              commit('setZResults', results);
            } else if (typeof results === 'object') {
              commit('setZResults', [results]);
              dispatch('getPropertyDetails', results.zpid);
            }
          }
        })
        .catch((err) => {
          commit('setErrored', true);
          console.log('Error in getSearchResult', err);
        })
        .finally(() => commit('setLoading', false));
    },

    getAttomAssessment({commit}, payload) {
      return AttomApiService.getAttomData('assessment/detail', payload)
        .then(({
          data
        }) => {
          if (!data || !data.status ||
            data.status.code == undefined || data.status.code * 1 == 400) {
            return;
          }
          var results = data.property[0];
          commit('setAttomAssessment', results);
        })
        .catch((err) => {
        })
        .finally(() => {
        });
    },

    getAttomSaleHistory({commit}, payload) {
      return AttomApiService.getAttomData('saleshistory/expandedhistory', payload)
        .then(({
          data
        }) => {
          if (!data || !data.status ||
            data.status.code == undefined || data.status.code * 1 == 400) {
            return;
          }
          var results = data.property[0];
          commit('setAttomSalesHistory', results);
        })
        .catch((err) => {
        })
        .finally(() => {
        });
    },
  }
});
