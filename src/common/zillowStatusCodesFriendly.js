var statusCodes = {
  0: 'Request success!',
  1: 'Sorry! Service error! Try another?',
  2: 'Sorry! Could\'nt find results for that address. Try another?',
  3: 'Sorry! That information is currently unavailable. Please come back later and try again.',
  4: 'Sorry! That information is currently unavailable. Please come back later and try again.',
  500: 'Sorry! Invalid or missing address.	If including a city name, make sure to include the state too.',
  501: 'Sorry! Invalid or missing address.	If including a city name, make sure to include the state too.',
  502: 'Sorry! No results found. Try another?',
  503: 'Sorry! Failed to find that city, state or ZIP code.	Please check to see if the city/state/ZIP is valide.',
  504: 'Sorry! No coverage for this area!',
  505: 'Sorry! Your request timed out. The server could be busy or unavailable. Try again later?',
  506: 'Sorry! Address given is too long. If address is valid, try using abbreviations!',
  507: 'Sorry! No exact match found. Try re-checking that address.',
  508: 'Sorry! No exact match found. Try re-checking that address.'
};
export default statusCodes;
