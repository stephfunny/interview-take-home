import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
require('dotenv').load();

const ATTOM_API_KEY = process.env.VUE_APP_ATTOM_API_KEY;

var AttomApiService = {
    init() {
      Vue.use(VueAxios, axios);
      Vue.axios.defaults.headers.common['apikey'] = ATTOM_API_KEY;
      Vue.axios.defaults.headers.common['Accept'] = 'application/json';
    },

    getAttomData(resource, p) {
      var params = {};
      params.address1 = p.address1;
      params.address2 = p.address2;
      return Vue.axios.get(`https://cors-anywhere.herokuapp.com/https://search.onboard-apis.com/propertyapi/v1.0.0/${resource}`,
        {params: params})
        // ,{headers:['apikey', ATTOM_API_KEY]})
        .catch(error => {
        throw new Error(`Attom ApiService ${error}`);
      });
    },
  };

  export default AttomApiService;
