import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
require('dotenv').load();

const ZILLOW_API_KEY = process.env.VUE_APP_ZILLOW_API_KEY;

var ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = 'https://cors-anywhere.herokuapp.com/http://www.zillow.com/webservice/';
  },

  getZillowData(resource, params) {
    params['zws-id'] = ZILLOW_API_KEY;
    params['rentzestimate'] = true;
    return Vue.axios.get(resource, {params: params}).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

};

export default ApiService;
